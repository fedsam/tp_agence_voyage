package com.agencetrip.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.agencetrip.dao.InsertDAO;
import com.agencetrip.dao.LocationDAO;
import com.agencetrip.models.Offer;

/**
 * 
 * @author vincent
 *
 * Permet de gérer l'ajout d'une offre
 * 
 */
@WebServlet(urlPatterns = "/employee/addoffer")
public class AddOffer extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("title", "Ajouter une offre");
		request.setAttribute("locations", new LocationDAO().getAll());
		this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/addOffer.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Test de l'existence des paramètres
		Integer idLocation = (!request.getParameter("location").isEmpty()) ?  Integer.parseInt(request.getParameter("location")) : null;
		String description = (!request.getParameter("description").isEmpty()) ? request.getParameter("description") : null;
		Integer pricePerPerson = (!request.getParameter("pricePerPerson").isEmpty()) ? Integer.parseInt(request.getParameter("pricePerPerson")) : null;
		Integer nbPersonMax = (!request.getParameter("nbPersonMax").isEmpty()) ? Integer.parseInt(request.getParameter("nbPersonMax")) : null;
		
		if (idLocation != null && description != null && pricePerPerson != null) {
			// Ajout de l'offre en base de données
			new InsertDAO().saveObject(new Offer(description, pricePerPerson, nbPersonMax, new LocationDAO().find(idLocation)));
			
			// Redirection vers la page d'accueil
			response.sendRedirect(request.getContextPath() + "/");
		} else {
			request.setAttribute("title", "Ajouter une offre");
			this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/addOffer.jsp").forward(request, response);
		}	
	}
}
