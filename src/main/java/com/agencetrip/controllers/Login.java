package com.agencetrip.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.agencetrip.dao.EmployeeDAO;
import com.agencetrip.models.Employee;

/**
 * 
 * @author vincent
 *
 * Permet de gérer la connexion d'un employée
 *
 */
@WebServlet(urlPatterns = "/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("title", "Connexion");
		this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Test de l'existence des paramètres
		String alias = (!request.getParameter("alias").isEmpty()) ? request.getParameter("alias") : null;
		String password = (!request.getParameter("password").isEmpty()) ? request.getParameter("password") : null;
		EmployeeDAO employeeDAO = new EmployeeDAO();
		Employee employee = null;
		
		if (alias != null && password != null) {
			// Vérifie si les identifiants correspondent à ceux en base de données
			employee = employeeDAO.checkCredentials(alias, password);
			
			// S'ils correspondent création d'une session utilisateur et redirection vers l'accueil
			if (employee != null) {
				HttpSession session = request.getSession(true);
	            session.setAttribute("employee_id", employee.getIdEmployee());
	            response.sendRedirect(request.getContextPath() + "/");
	            
            // Sinon réaffichage de la page
			} else {
				request.setAttribute("title", "Connexion");
	            request.setAttribute("alias", alias);
	            this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
			}
		}
	}

}
