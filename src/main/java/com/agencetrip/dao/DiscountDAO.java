package com.agencetrip.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.agencetrip.models.Discount;

/**
 * 
 * @author vincent
 *
 * Permet de gérer les actions CRUD pour l'objet Discount
 * @see Discount
 *
 */
public class DiscountDAO {
	/**
	 * Récupère toutes les remises par tranche d'âge
	 * @return liste des remises
	 */
	public List<Discount> getAll() {
		List<Discount> discounts = null;
		EntityManager entityManager = null;

		try {
			entityManager = DAOSingleton.getEntityManagerFactory().createEntityManager();

			EntityTransaction transaction = entityManager.getTransaction();
			transaction.begin();
			
			discounts = entityManager.createQuery("from Discount", Discount.class).getResultList();
			
			transaction.commit();
		} finally {
			if (entityManager != null) {
				entityManager.close();
			}
		}
		return discounts;
	}
}
