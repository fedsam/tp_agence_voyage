package com.agencetrip.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class InsertDAO {
	public void saveObject(Object object) {
		EntityManager entityManager = null;

		try {
			// Creation d'un entityManager à partir d'une factory
			entityManager = DAOSingleton.getEntityManagerFactory().createEntityManager();

			EntityTransaction transaction = entityManager.getTransaction();
			// Début transaction
			transaction.begin();
			
			// Sauvegarde de l'objet
			entityManager.persist(object);
			
			// Valide la transaction
			transaction.commit();
		} finally {
			if (entityManager != null) {
				entityManager.close();
			}
		}
	}
}
