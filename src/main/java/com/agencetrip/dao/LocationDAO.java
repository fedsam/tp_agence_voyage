package com.agencetrip.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.agencetrip.models.Location;

/**
 * 
 * @author vincent
 *
 * Permet de gérer les actions CRUD de l'objet Location
 *
 * @see Location
 *
 */
public class LocationDAO {
	/**
	 * Récupère tous les lieux
	 * @return Liste des lieux
	 */
	public List<Location> getAll() {
		List<Location> locations = null;
		EntityManager entityManager = null;

		try {
			entityManager = DAOSingleton.getEntityManagerFactory().createEntityManager();

			EntityTransaction transaction = entityManager.getTransaction();
			transaction.begin();
			
			locations = entityManager.createQuery("from Location", Location.class).getResultList();
			
			transaction.commit();
		} finally {
			if (entityManager != null) {
				entityManager.close();
			}
		}
		return locations;
	}
	
	/**
	 * Récupère le lieu selon son identifiant
	 * @param id
	 * @return le lieu qui correspond à l'id passé en argument
	 */
	public Location find(Integer id) {
		EntityManager entityManager = null;
		Location location = null;
		if (id != null) {
			try {
				entityManager = DAOSingleton.getEntityManagerFactory().createEntityManager();

				EntityTransaction transaction = entityManager.getTransaction();
				transaction.begin();
				
				location = entityManager.find(Location.class, id);
				
				transaction.commit();
			} finally {
				if (entityManager != null) {
					entityManager.close();
				}
			}
		}
		return location;
	}
}
