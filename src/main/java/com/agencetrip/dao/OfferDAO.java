package com.agencetrip.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.agencetrip.models.Offer;

/**
 * 
 * @author vincent
 *
 * Permet de gérer les actions CRUD de l'objet Offer
 * 
 * @see Offer
 *
 */
public class OfferDAO {
	/**
	 * Récupère l'offre correspond à l'identifiant donné
	 * @param id
	 * @return l'offre qui correspond à l'id | null sinon
	 */
	public Offer find(Integer id) {
		Offer offer = null;
		EntityManager entityManager = null;

		try {
			entityManager = DAOSingleton.getEntityManagerFactory().createEntityManager();

			EntityTransaction transaction = entityManager.getTransaction();
			transaction.begin();
			
			offer = entityManager.find(Offer.class, id);
			
			transaction.commit();
		} finally {
			if (entityManager != null) {
				entityManager.close();
			}
		}
		return offer;
	}
	
	/**
	 * Récupère toutes les offres
	 * @return liste des offres | null sinon
	 */
	public List<Offer> getAll() {
		List<Offer> offers = null;
		EntityManager entityManager = null;

		try {
			entityManager = DAOSingleton.getEntityManagerFactory().createEntityManager();

			EntityTransaction transaction = entityManager.getTransaction();
			transaction.begin();
			
			offers = entityManager.createQuery("from Offer", Offer.class).getResultList();
			
			transaction.commit();
		} finally {
			if (entityManager != null) {
				entityManager.close();
			}
		}
		return offers;
	}
}
