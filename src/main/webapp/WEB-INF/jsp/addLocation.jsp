<%@page import="java.util.ArrayList"%>
<%@page import="com.agencetrip.models.Offer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!doctype html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="assets/css/index.css" type="text/css">
<title><%= request.getAttribute("title") %></title>
</head>
<body>
	<div class="content">
		<h1>Agencetrip</h1>
		<nav>
			<%
				if (request.getAttribute("isConnected") != null) {
					out.println("<a href=\"logout\">Déconnexion</a>");
					out.println("<a href=\"/agencetrip\">Accueil</a>");
					out.println("<a href=\"addoffer\">Ajouter une offre</a>");
				} else {
					out.println("<a href=\"login\">Connexion</a>");
				}
			%>
		</nav>
		 <hr>
		<h2>Ajouter un Lieu</h2>
			<form action="addlocation" method="post">
				<label>Nom du lieu</label>
				<input name="name" type="text" required>
				<br>
				<select name="country" required>
					<option>France</option>
					<option>Italie</option>
					<option>Espagne</option>
					<option>Belgique</option>
				</select>
				<br>
				<label>Description des endroits à visiter</label>
				<br>
				<textarea name="visits" rows="4" cols="40" required></textarea>
				<br>
				<label>Description des activités</label>
				<br>
				<textarea name="activities" rows="4" cols="40" required></textarea>
				<br>
				<br>
				<input type="submit" value="Ajouter">
			</form>
	</div>
</body>
</html>