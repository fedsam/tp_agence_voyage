<%@page import="java.util.ArrayList"%>
<%@page import="com.agencetrip.models.Offer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!doctype html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="assets/css/index.css" type="text/css">
<title><%= request.getAttribute("title") %></title>
</head>
<body>
	<div class="content">
		<h1>Agencetrip</h1>
		<nav>
			<%
				if (request.getAttribute("isConnected") != null) {
					out.println("<a href=\"employee/logout\">Déconnexion</a>");
					out.println("<a href=\"employee/addoffer\">Ajouter une offre</a>");
					out.println("<a href=\"employee/addlocation\">Ajouter un lieu</a>");
				} else {
					out.println("<a href=\"login\">Connexion</a>");
				}
			%>
		</nav>
		 <hr>
		<h2>Merci d'avoir réservé sur notre site</h2>
		<p>Un email vous a été envoyer afin de procéder au paiement d'un montant de : <%= request.getAttribute("price") %>€</p>
		
		<p><a href="/agencetrip">Retourner à l'accueil</a></p>
	</div>
</body>
</html>